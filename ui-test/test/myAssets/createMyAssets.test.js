import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath,
    assertButtonEnable,
    assertToBeTruthy,
    deleteModelEcrRepo,
    clickButtonByDraftandPublish
} from '../../utils';

const capabilities = getCapabilities('create myassets');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    const random = Math.random().toString(36).substring(7);;
    const MODEL_NAME = 'test_'+ random;

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });

    test(
        'create myassets',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.,"Register New Model")]');
            //check field model url
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelUrl"]',
                'hackathon-test-user/mobilenet.tar.gz'
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelName"]',
                MODEL_NAME
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//textarea[@formcontrolname="description"]',
                'desc'
            );
            //check field License
            await clickButtonByPath(
                driver, 
                '//pg-select[@ng-reflect-name="license"]'
            );
            //click label License
            await clickButtonByPath(
                driver, 
                '//li[contains(.,"Apache")]'
            );
            //click next
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Next ")]'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.,"Downloading Model")]',
                'Downloading Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Building Model ")]',
                'Building Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Testing Model ")]',
                'Testing Model'
            );
            await assertToEqual(
                driver,
                '//h5[contains(.," Storing Model ")]',
                'Storing Model'
            );
            //assert button enable
            await assertButtonEnable(
                driver,
                 '//button[contains(.," View Draft and Publish ")]'
            );
            //click view draft
            await clickButtonByDraftandPublish(
                driver, 
                '//button[contains(.," View Draft and Publish ")]'
            );             
            //click publish model
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Publish Model ")]'
            );
            //assert button publish success
            await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/pgcard/div/div[1]/div[contains(.,"'+MODEL_NAME+'")]',
            );
        },130000
    );

});
