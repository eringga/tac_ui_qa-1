import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath
} from '../../utils';

const capabilities = getCapabilities('Test Lucerne Login');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    test(
        'Lucerne login',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
             //click menu engine
             await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
        },80000
    );

});