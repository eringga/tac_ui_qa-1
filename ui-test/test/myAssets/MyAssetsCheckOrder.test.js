import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath,
    assertButtonEnable,
    assertToBeTruthy
} from '../../utils';

const capabilities = getCapabilities('myassets check order');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    test(
        'myassets check order',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click order
            await clickButtonByPath(driver, '//pg-select[@ng-reflect--place-holder="Select Option"]');
            //select oldest first
            await clickButtonByPath(driver, '//li[contains(.," Oldest First ")]');
            //check data by Oldest First
            await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/div/pgcard/div/div[1]/div',            
            );
            //click order
            await clickButtonByPath(driver, '//pg-select[@ng-reflect--place-holder="Select Option"]');
            //select Alphabetical
            await clickButtonByPath(driver, '//li[contains(.," Alphabetical ")]');
             //check data by Alphabetical
             await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/div/pgcard/div/div[1]/div',            
            );
            //click order
            await clickButtonByPath(driver, '//pg-select[@ng-reflect--place-holder="Select Option"]');
            //select most recent
            await clickButtonByPath(driver, '//li[contains(.," Most Recent ")]');
             //check data
             await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/div/pgcard/div/div[1]/div',            
            );
        },130000
    );

});