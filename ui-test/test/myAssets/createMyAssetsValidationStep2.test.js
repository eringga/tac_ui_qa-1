import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath,
    deleteModelEcrRepo,
    assertButtonDisable
} from '../../utils';

const capabilities = getCapabilities('create myassets check validation step 2');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    const random = Math.random().toString(36).substring(7);;
    const MODEL_NAME = 'test_'+ random;

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });

    test(
        'create myassets check validation step 2',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.,"Register New Model")]');
            //check field model url
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelUrl"]',
                'hackathon-test-user/test-model.tar.gz'
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelName"]',
                MODEL_NAME
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//textarea[@formcontrolname="description"]',
                'desc test error step 2'
            );
            //check field License
            await clickButtonByPath(
                driver, 
                '//pg-select[@ng-reflect-name="license"]'
            );
            //click label License
            await clickButtonByPath(
                driver, 
                '//li[contains(.,"Apache")]'
            );
            //click next
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Next ")]'
            );
            //assert error step 2
            await assertToEqual(
                driver,
                "//div[@ng-reflect-klass='alert alert-danger']/div",
                "Error building model: 'NoneType' object is not subscriptable"
            );
            //assert button disabled
            await assertButtonDisable(
                driver,
                 '//button[contains(.," View Draft and Publish ")]'
            );
        },130000
    );

});