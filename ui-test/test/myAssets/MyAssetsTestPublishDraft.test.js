import { 
    getCapabilities, 
    Login, 
    ConfigWebdriver, 
    SauceLabsConfig,
    clickButtonByPath,
    assertToEqual,
    formInputByPath,
    assertButtonEnable,
    deleteModelEcrRepo,
    assertToBeTruthy
} from '../../utils';

const capabilities = getCapabilities('myassets test publish draft');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_APVM);

    const random = Math.random().toString(36).substring(7);;
    const MODEL_NAME = 'test_'+ random;

    afterAll(() => {
        return deleteModelEcrRepo(MODEL_NAME);
    });


    test(
        'myassets test publish draft',
        async () => {
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //click menu engine
            await clickButtonByPath(driver, '//span[contains(.,"Engines")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.," My Assets")]');
            //click menu myassets
            await clickButtonByPath(driver, '//a[contains(.,"Register New Model")]');
            //check field model url
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelUrl"]',
                'hackathon-test-user/mobilenet.tar.gz'
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//input[@formcontrolname="modelName"]',
                MODEL_NAME
            );
            //check field model name
            await formInputByPath(
                driver, 
                '//textarea[@formcontrolname="description"]',
                'desc'
            );
            //check field License
            await clickButtonByPath(
                driver, 
                '//pg-select[@ng-reflect-name="license"]'
            );
            //click label License
            await clickButtonByPath(
                driver, 
                '//li[contains(.,"Apache")]'
            );
            //click next
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Next ")]'
            );
            //click view draft
            await clickButtonByPath(
                driver, 
                '//a[contains(.,"Go to My Assets")]'
            ); 
            //click view edit draft
            await clickButtonByPath(
                driver, 
                '//div[@class="card-body model-card-body"]/div/div[1]/div/pgcard/div/div[2]/div[2]/button[contains(.," Edit Draft ")]'
            );      
            //assert title
            await assertToEqual(
                driver,
                '//h1[contains(.,"'+MODEL_NAME+'")]',
                MODEL_NAME
            ); 
            //click publish model
            await clickButtonByPath(
                driver, 
                '//button[contains(.," Publish Model ")]'
            );
            //assert button publish success
            await assertToBeTruthy(
                driver,
                '//div[@class="col-lg-4 ng-star-inserted"]/pgcard/div/div[1]/div[contains(.,"'+MODEL_NAME+'")]',
            );
        },130000
    );

});