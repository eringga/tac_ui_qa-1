import webdriver from 'selenium-webdriver';
import {
    getElementByXpath,
    getCapabilities,
    Login,
    ConfigWebdriver,
    SauceLabsConfig,
    dovmChooseExpansionMenu,
    setFormInput,
    clickRadioButtonById,
    clickButtonByPath,
    selectOptionById,
    expectToBeTruthyPath,
    selectOptionByIdPath
  } from "../../utils";

const capabilities = getCapabilities('TAC test datasets', 'DOVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

    test(
        'dovm-datasets',
        async () => {
            //get login
            await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
            //get element data request
            const output = await getElementByXpath(
            driver,
            "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
            );
            const outputVal = await output.getText();
            expect(outputVal).toEqual("Data Requests");
            //click dataset menu
            await dovmChooseExpansionMenu(driver, '//mat-expansion-panel/div[@id="cdk-accordion-child-1"]/mat-action-row[2]/button')
            // click button define new dataset
            await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button')
            // click select database and choose option value
            await selectOptionByIdPath(driver, 'mat-select-4', '//mat-option/span[contains(.,"mydb9")]');
            // click select address and choose option value
            await selectOptionByIdPath(driver, 'mat-select-5', '//mat-option/span[contains(.,"209.97.173.183")]');
            // click select port and choose option value
            await selectOptionByIdPath(driver, 'mat-select-6', '//mat-option/span[contains(.,"5432")]');
            // click select database user and choose option value
            await selectOptionByIdPath(driver, 'mat-select-7', '//mat-option/span[contains(.,"eringga")]');
            // click input table name and set value
            await setFormInput(driver, 'mat-input-5', 'table_watermarking')
            // click input description and set value
            await setFormInput(driver, 'mat-input-6', 'table_watermarking dataset2')
            // click select restriction preference nad choose option value
            await selectOptionByIdPath(driver, 'mat-select-8', '//mat-option/span[contains(.,"Full Disclosure")]');
            // click input price and set value
            await setFormInput(driver, 'mat-input-7', '50')
            // click radio button obfuscate
            await clickRadioButtonById(driver, 'mat-checkbox-1')
            // click radio button anomaly detection
            await clickRadioButtonById(driver, 'mat-radio-3')
            // click add button
            await clickButtonByPath(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-dataset-dialog/form/div[3]/button')
            // click yes button
            const buttonYes = await getElementByXpath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');
            buttonYes.click();
            expect(buttonYes).toBeTruthy()
        },160000
    );

});