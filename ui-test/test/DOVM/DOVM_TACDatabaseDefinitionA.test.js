import webdriver from "selenium-webdriver";

import {
 getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig, clickButtonByPath, clickButtonById, expectToBeTruthy
} from '../../utils';

const capabilities = getCapabilities("TAC test databases definition A", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-database definition A", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");
    //click database menu
    await clickButtonById(driver, 'mat-expansion-panel-header-1');
    //click element menu database
    await clickButtonByPath(driver, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
    // click configure new database
    await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button')
    // click button add
    await clickButtonByPath(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-database-dialog/form/div/button')
    // expect database name input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-0') 
    // expect database description input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-1')
    // expect database type select element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-2')
    // expect database version select element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-3') 
    // expect address input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-4')
    // expect port input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-5')
    // expect database user input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-6')
    // expect password input element error alert to be shown
    await expectToBeTruthy(driver, 'mat-error-7')

  }, 150000);
});
