import webdriver from "selenium-webdriver";

import {
  getElementByXpath,
  getCapabilities,
  Login,
  ConfigWebdriver,
  SauceLabsConfig,
  clickButtonById,
  clickButtonByPath
} from "../../utils";

const capabilities = getCapabilities("DOVM test review data request details on pending request", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-dataRequest details review on pending request", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");

    // click plus button
    await clickButtonByPath(driver, '//mat-tab-body[@id="mat-tab-content-0-0"]/div/div[2]/mat-table/mat-row[1]/mat-cell[1]/button')
    // click preview the list of all libraries
    await clickButtonById(driver, 'mat-expansion-panel-header-4')
    // click dataset details tab
    await clickButtonById(driver, 'mat-tab-label-1-1')

  }, 160000);
});
