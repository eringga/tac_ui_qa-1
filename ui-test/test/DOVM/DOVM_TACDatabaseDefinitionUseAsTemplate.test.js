import webdriver from "selenium-webdriver";

import {
  helper,
  getElementById,
  getElementByName,
  getElementByXpath,
  getCapabilities,
  Login,
  Logout,
  ConfigWebdriver,
  SauceLabsConfig,
  clickButtonById,
  clickButtonByPath,
  getText,
  setFormInput
} from "../../utils";

const capabilities = getCapabilities("TAC test database definition use as template", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-database definition use as template", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");
    //click element menu data
    await clickButtonById(driver, "mat-expansion-panel-header-1")    
    // click element menu database
    await clickButtonByPath(driver, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button')
    // click side option button
    await clickButtonByPath(driver, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/div/div[2]/mat-table/mat-row[1]/mat-cell[7]/button')
    // click use as template
    const UseTemplate = await getElementByXpath(driver, '//div[@id="cdk-overlay-0"]/div/div/button[2]');
    UseTemplate.click();
    expect(UseTemplate).toBeTruthy()
    //assert db name
    const dbName = await getElementById(driver, 'mat-input-5');
    const dbNameCheck = await dbName.getAttribute("value");
    expect(dbNameCheck).not.toBeNull()
    //assert db description
    const dbDesc = await getElementById(driver, 'mat-input-6');
    const dbDescCheck = await dbDesc.getAttribute("value");
    expect(dbDescCheck).not.toBeNull()
    //element set form input database type
    const dbType = await getElementByXpath(driver, '//mat-select[@id="mat-select-4"]/div/div/span/span');
    const dbTypeCheck = await dbType.getText();
    expect(dbTypeCheck).not.toBeNull()
    //element set form input database version
    const dbVersion = await getElementByXpath(driver, '//mat-select[@id="mat-select-5"]/div/div/span/span');
    const dbVersionCheck = await dbVersion.getText();    
    expect(dbVersionCheck).not.toBeNull()
    //element set form input address
    const address = await getElementById(driver, 'mat-input-7');
    const addressCheck = await address.getAttribute("value");
    expect(addressCheck).not.toBeNull()
    //element set form input port
    const port = await getElementById(driver, 'mat-input-8');
    const portCheck = await  port.getAttribute("value");
    expect(portCheck).not.toBeNull()
    //element set form input database user
    const dbUser = await getElementById(driver, 'mat-input-9');
    const dbUserCheck = await dbUser.getAttribute("value");
    expect(dbUserCheck).not.toBeNull()
    //check password
    const pwd = await getElementById(driver, 'mat-input-10');
    const pwdCheck = await pwd.getAttribute("value");
    expect(pwdCheck).toEqual("");
    //set password 
    await setFormInput(driver, 'mat-input-10', 'guest10')
    //click button add
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]')
    //click button yes
    const buttonYes = await getElementByXpath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');
    buttonYes.click();
    expect(buttonYes).toBeTruthy()
    
  }, 130000);
});
