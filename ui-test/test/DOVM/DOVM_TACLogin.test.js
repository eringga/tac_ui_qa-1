import webdriver from 'selenium-webdriver';

import { helper, getElementById, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig } from '../../utils';

const capabilities = getCapabilities('TAC Login', 'DOVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

    test(
        'dovm-login',
        async () => {
            await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
        },30000
    );

});
