import webdriver from "selenium-webdriver";

import { 
  helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig,
  SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath, 
  selectOptionById, getText 
} from '../../utils';

const capabilities = getCapabilities("TAC test databases definition use as template", "DOVM");

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_DOVM);

  test("dovm-database definition use as template", async () => {
    await Login(driver, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
    //get element data request
    const output = await getElementByXpath(
      driver,
      "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span"
    );
    const outputVal = await output.getText();
    expect(outputVal).toEqual("Data Requests");
    //click database menu
     await clickButtonById(driver, 'mat-expansion-panel-header-1');
     //click element menu database
     await clickButtonByPath(driver, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');  
     //click data
    await clickButtonByPath(driver, '//mat-row[@class="mat-row ng-star-inserted"]/mat-cell[1]/button[@class="float-right mat-icon-button"]');
    //click element add button and click button us as template
    await clickButtonByPath(driver, '//button[@class="float-right mat-raised-button menu-button-orange"]');
    //Fill input password elemnts
    await setFormInput(driver, 'mat-input-10', 'Test123')

  }, 150000);
});
