import {
    getElementByXpath, getCapabilities, Login, SauceLabsConfig,
    SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath,
    selectOptionById, getText, selectOptionByPath, formInputByPath
} from '../../utils';

const capabilities = getCapabilities('TAC Create New Contract 1 Review Authorized Contracts', 'APVM');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);
    let driver2 = SauceLabsConfig(capabilities);
    // ConfigWebdriver(driver2, process.env.SITE_URL_DOVM);
    ConfigWebdriverDual(driver, driver2, process.env.SITE_URL_APVM_NO_MARKET, process.env.SITE_URL_DOVM)

    test(
        'dovm-create new database and datasets',
        async () => {
            await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
            //get element data request
            const output = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
            const outputVal = await output.getText();
            expect(outputVal).toEqual("Data Requests");
            //click element menu data
            await clickButtonById(driver2, 'mat-expansion-panel-header-1');
            //click element menu database
            clickButtonByPath(driver2, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
            //click element button configure new database
            await clickButtonByPath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
            //element set form input database name
            await formInputByPath(driver2, '//input[@placeholder="Database Name"]', 'mydb11');
            //element set form input database description
            await formInputByPath(driver2, '//input[@placeholder="Database Description"]', 'database test')
            //element set form input database type
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Type")]', '//mat-option/span[contains(.,"Postgres")]')
            //element set form input database version
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database Version")]', '//mat-option/span[contains(.,"9.5")]')
            //element set form input address
            await formInputByPath(driver2, '//input[@placeholder="Address"]', '209.97.173.183');
            //element set form input port
            await formInputByPath(driver2, '//input[@placeholder="Port"]', '5432');
            //element set form input database user
            await formInputByPath(driver2, '//input[@placeholder="Database User"]', 'eringga');
            //element set pawword form input
            await formInputByPath(driver2, '//input[@placeholder="Password"]', 'makeithappen');
            // click element button add
            clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
            // click element button yes
            await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/button[2]');
            // wait until database created
            await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
            // await getElementByXpath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/div/span')
            // click close button
            await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/mat-toolbar/button');

            // click datasets
            await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[2]/button');
            // click define new datasets
            await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
            // click select database and choose option value
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database")]', '//mat-option/span[contains(.,"mydb11")]');
            // click select address and choose option value
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Address")]', '//mat-option/span[contains(.,"209.97.173.183")]');
            // click select port and choose option value
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Port")]', '//mat-option/span[contains(.,"5432")]');
            // click select database user and choose option value
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Database User")]', '//mat-option/span[contains(.,"eringga")]');
            // click input table name and set value
            await formInputByPath(driver2, '//input[@placeholder="Table Name"]', 'dataset_revauthorized')
            // click input description and set value
            await formInputByPath(driver2, '//input[@placeholder="Description"]', 'dataset revauthauthorized test')
            // click select restriction preference nad choose option value
            await selectOptionByPath(driver2, '//mat-select/div/div[1]/span[contains(.,"Restriction Preference")]', '//mat-option/span[contains(.,"Full Disclosure")]');
            // click input price and set value
            await formInputByPath(driver2, '//input[@placeholder="Price"]', '50')
            // click add button
            await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/form/div[3]/button')
            // click yes button
            await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
            // wait element to be displayed 
            await getElementByXpath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
            // click close button
            await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/mat-toolbar/button');
        }, 190000);

    test(
        'apvm-create new contract 1 authorized pending contracts',
        async () => {
            //get login         
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //get element text Analytics Contracts
            await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
            // Get Button new Contract and click
            await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
            // set form input name
            await setFormInput(driver, "mat-input-4", "test create new contract 1-review authorized contracts");
            // set form input description
            await setFormInput(driver, "mat-input-5", "description test create new contract 1-review authorized contracts");
            //set date
            await setDate(driver);
            //click next step 1
            await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
            // set element select value dataset 1
            await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"dataset revauthauthorized test")]');
            //click button next step2
            await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');
            //select value env conda
            await selectOptionById(driver, 'mat-select-4', 'mat-option-11');
            //click step 3
            await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
            //click step 4
            await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
            //click submited
            await getText(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-contract-dialog/button[@class="menu-button float-right mat-raised-button"]/span[@class="mat-button-wrapper"]/span[@class="menu-button-caption"]', 'SUBMITTED');
        }, 190000
    );

    test(
        'dovm-review authorized contracts',
        async () => {
            // //get login         
            // await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
            // //get element text Data Requests
            // await getText(driver2, '//span[@class="toolbar-title"]', 'Data Requests');
            //click data request menu
            await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[3]/button/span/span')
            //set element search
            await setFormInput(driver2, "filterInput", "test create new contract 1-review authorized contracts");
            //click actions
            await clickButtonByPath(driver2, '//mat-cell[@class="mat-cell cdk-column-options mat-column-options ng-star-inserted"]/button[@class="mat-raised-button menu-button-orange"]');
            //click next contract details
            await clickButtonByPath(driver2, '//div[@id="cdk-step-content-0-0"]/div[@class="ng-star-inserted"]/button[@class="mat-raised-button menu-button-orange float-right"]');
            //click next dataset details
            await clickButtonByPath(driver2, '//div[@id="cdk-step-content-0-1"]/div[@class="ng-star-inserted"]/button[@class="mat-raised-button menu-button-orange float-right"]');
            //click authorized data pending
            await clickButtonByPath(driver2, '//mat-toolbar[@class="mat-toolbar mat-toolbar-single-row ng-star-inserted"]/button[2]');
            //click yes
            await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
            //click authorized
            await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
            //click authorized menu
            await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-tab-group/mat-tab-header/div[2]/div/div/div[2]/div/span[contains(.,"AUTHORIZED REQUESTS")]');
            //set element search authorized
            await formInputByPath(driver2, "//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-tab-group/div/mat-tab-body[2]/div/div[1]/mat-form-field/div/div[1]/div/input", "test create new contract 1-review authorized contracts");
        }, 220000
    );

});