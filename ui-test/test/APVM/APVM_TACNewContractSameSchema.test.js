import webdriver from "selenium-webdriver";

import {
  helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig,
  SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath,
  selectOptionById, getText, expectToEqualPath
} from '../../utils';

const capabilities = getCapabilities(
  "TAC New Contract 2 Datasets Same Scehma",
  "APVM CreateNewContractSameSchema"
);

describe("webdriver", () => {

  let driver = SauceLabsConfig(capabilities);
  let driver2 = SauceLabsConfig(capabilities);
  ConfigWebdriverDual(driver, driver2, process.env.SITE_URL_APVM_NO_MARKET, process.env.SITE_URL_DOVM);

  test(
    'dovm-create new database and datasets',
    async () => {
      await Login(driver2, process.env.USERNAME_DOVM, process.env.PASSWORD_DOVM);
      //get element data request
      const output = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-data-request/mat-toolbar/span');
      const outputVal = await output.getText();
      expect(outputVal).toEqual("Data Requests");
      //click element menu data
      const clickData = await getElementById(driver2, 'mat-expansion-panel-header-1');
      await clickData.click();
      //click element menu database
      const clickDatabase = await getElementByXpath(driver2, '//div[@id="cdk-accordion-child-1"]/mat-action-row[1]/button');
      await clickDatabase.click();
      //click element button configure new database
      const clicknewdatabase = await getElementByXpath(driver2, '//html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-database/mat-toolbar/button');
      await clicknewdatabase.click();
      //element set form input database name
      const dbName = await getElementById(driver2, 'mat-input-5');
      await dbName.clear();
      await dbName.sendKeys("mydb4");
      //element set form input database description
      const dbDesc = await getElementById(driver2, 'mat-input-6');
      await dbDesc.clear();
      await dbDesc.sendKeys("database test");
      //element set form input database type
      const dbType = await getElementById(driver2, 'mat-select-4')
      await dbType.click();
      const dbTypeVal = await getElementById(driver2, 'mat-option-17')
      await dbTypeVal.click();
      //element set form input database version
      const dbVersion = await getElementById(driver2, 'mat-select-5')
      await dbVersion.click();
      const dbVersionVal = await getElementById(driver2, 'mat-option-20')
      await dbVersionVal.click();
      //element set form input address
      const address = await getElementById(driver2, 'mat-input-7');
      await address.clear();
      await address.sendKeys("209.97.173.183");
      //element set form input port
      const port = await getElementById(driver2, 'mat-input-8');
      await port.clear();
      await port.sendKeys("5432");
      //element set form input database user
      const dbUser = await getElementById(driver2, 'mat-input-9');
      await dbUser.clear();
      await dbUser.sendKeys("eringga");
      //element set pawword form input
      const pwd = await getElementById(driver2, 'mat-input-10');
      await pwd.clear();
      await pwd.sendKeys("makeithappen");
      //click element button add
      const add = await getElementByXpath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      await add.click();
      //click element button yes
      const yes = await getElementByXpath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait until database created
      await yes.click();
      await getElementByXpath(driver2, '//html/body/div[2]/div[2]/div/mat-dialog-container/configure-database-dialog/div/button/span/span[contains(.,"ADDED")]')
      //click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-0"]/configure-database-dialog/mat-toolbar/button');

      // create new datasets 1
      // click datasets
      await clickButtonByPath(driver2, '//*[@id="cdk-accordion-child-1"]/mat-action-row[2]/button');
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-7', '//mat-option/span[contains(.,"mydb4")]');
      // click select address and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-8', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-9', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-10', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await setFormInput(driver2, 'mat-input-12', 'dataset_full')
      // click input description and set value
      await setFormInput(driver2, 'mat-input-13', 'first dataset test')
      // click select restriction preference nad choose option value
      await selectOptionByIdPath(driver2, 'mat-select-11', '//mat-option/span[contains(.,"Full Disclosure")]');
      // click input price and set value
      await setFormInput(driver2, 'mat-input-14', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // click close button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-1"]/configure-dataset-dialog/mat-toolbar/button');

      // create new datasets 2
      // click define new datasets
      await clickButtonByPath(driver2, '/html/body/app-root/main/app-main/div/mat-sidenav-container/mat-sidenav-content/div/app-dataset/mat-toolbar/button');
      // click select database and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-12', '//mat-option/span[contains(.,"mydb4")]');
      // click select address and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-13', '//mat-option/span[contains(.,"209.97.173.183")]');
      // click select port and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-14', '//mat-option/span[contains(.,"5432")]');
      // click select database user and choose option value
      await selectOptionByIdPath(driver2, 'mat-select-15', '//mat-option/span[contains(.,"eringga")]');
      // click input table name and set value
      await setFormInput(driver2, 'mat-input-16', 'dataset_full')
      // click input description and set value
      await setFormInput(driver2, 'mat-input-17', 'second dataset test')
      // click select restriction preference nad choose option value
      await selectOptionByIdPath(driver2, 'mat-select-16', '//mat-option/span[contains(.,"Full Disclosure")]');
      // click input price and set value
      await setFormInput(driver2, 'mat-input-18', '50')
      // click add button
      await clickButtonByPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/form/div[3]/button')
      // click yes button
      await clickButtonByPath(driver2, '//button[@class="float-right mat-raised-button menu-button-orange"]');
      // wait element to be displayed 
      await getElementByXpath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]')
      // expect to equal dataset added
      await expectToEqualPath(driver2, '//*[@id="mat-dialog-2"]/configure-dataset-dialog/div/span[contains(.,"Dataset Added")]', 'Dataset Added')
    }, 280000
  );

  test("apvm-create new contract 2 datasets same schema", async () => {
    // Login
    await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
    //get element text analytics contracts
    await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
    // Get contract button and click
    await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
    // set element form input name
    await setFormInput(driver, 'mat-input-4', "test new contract 2 datasets same schema");
    //set element form input description
    await setFormInput(driver, 'mat-input-5', "description test new contract 2 dataset with same schema");
    // set element form input date
    await setDate(driver);
    // click button next step1
    await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
    // set element form select option dataset1
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"first dataset test")]');
    // set element form select option dataset2 same schema value
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"second dataset test")]');
    //click button next step2
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');
    //select value env conda
    await selectOptionById(driver, 'mat-select-4', 'mat-option-11');
    //click step 3
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
    //click step 4
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
    // Get success alert and expect success alert text to equal SUBMITTED
    await getText(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-contract-dialog/button[@class="menu-button float-right mat-raised-button"]/span[@class="mat-button-wrapper"]/span[@class="menu-button-caption"]', 'SUBMITTED');
  }, 120000);
});