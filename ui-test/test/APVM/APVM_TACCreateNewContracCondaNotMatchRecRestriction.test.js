import webdriver from "selenium-webdriver";

import { 
  helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig,
  SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath, 
  selectOptionById, getText 
} from '../../utils';

const capabilities = getCapabilities(
  "TAC New Contract 2 Datasets Same Scehma",
  "APVM CreateNewContractSameSchema"
);

describe("webdriver", () => {
  let driver = SauceLabsConfig(capabilities);
  ConfigWebdriver(driver, process.env.SITE_URL_APVM_NO_MARKET);


  test("create new contract 2 datasets same schema", async () => {
    // Login
    await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
    // Get Toolbar to show up
    await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
    // Get Button new Contract and click
    await clickButtonByPath(driver, '//button[@class="menu-button mat-raised-button"]');
    // set form input name
    await setFormInput(driver, "mat-input-4", "test new contract conda not match");
    // set form input description
    await setFormInput(driver, "mat-input-5", "description test new contract conda not match");
    //set date
    await setDate(driver);
    //click next step 1
    await clickButtonByPath(driver, '//button[@class="menu-button float-right mat-raised-button"]');
    // Fill Datasets form
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"table_watermarking dataset2")]');   
    await selectOptionByIdPath(driver, 'mat-select-3', '//mat-option/span/div/div[contains(.,"table_watermarking dataset2")]');
    //click next step 2
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-1"]/div/button[@class="menu-button float-right mat-raised-button"]');    
    // Fill Authorized Computations
    await selectOptionById(driver, 'mat-select-4', 'mat-option-11'); 
    //click restrict
    await selectOptionByIdPath(driver, 'mat-select-6', '//mat-option[@class="mat-option ng-star-inserted mat-selected mat-active"]');
    await selectOptionByIdPath(driver, 'mat-select-7', '//mat-option[@class="mat-option ng-star-inserted mat-selected mat-active"]');
    //click next step 3
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-2"]/div/button[@class="menu-button float-right mat-raised-button"]');
    // Get submit button and click
    await clickButtonByPath(driver, '//div[@id="cdk-step-content-0-3"]/div/button[@class="menu-button float-right mat-raised-button"]');
    // get success alert and expect success alert text to equal SUBMITTED
    await getText(driver, '//mat-dialog-container[@id="mat-dialog-0"]/configure-contract-dialog/button[@class="menu-button float-right mat-raised-button"]/span[@class="mat-button-wrapper"]/span[@class="menu-button-caption"]', 'SUBMITTED');        
  }, 180000);
});
