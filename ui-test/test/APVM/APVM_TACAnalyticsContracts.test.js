import webdriver from 'selenium-webdriver';

import { 
    helper, getElementById, start, stop, getElementByName, getElementByXpath, getCapabilities, Login, ConfigWebdriver, SauceLabsConfig,
    SauceLabsConfigDual, ConfigWebdriverDual, setFormInput, clickButtonByPath, clickButtonById, setDate, selectOptionByIdPath, 
    selectOptionById, getText, Logout
} from '../../utils';

var $ = require('jquery');

const capabilities = getCapabilities('APVM Analytics Contracts', 'APVM AnalyticsContracts');

describe('webdriver', () => {
    let driver = SauceLabsConfig(capabilities);    
    ConfigWebdriver(driver, process.env.SITE_URL_APVM_NO_MARKET);

    test(
        'apvm-authorized-contracts',
        async () => {
            //get login
            await Login(driver, process.env.USERNAME_APVM, process.env.PASSWORD_APVM);
            //get element text Analytics Contracts
            await getText(driver, '//span[@class="toolbar-title"]', 'Analytics Contracts');
        },30000
    );

    test(
        'apvm-pending-contracts',
        async () => {
            //click element button pending contracts
            await clickButtonById(driver, 'mat-tab-label-0-0');
            //get logout
            await Logout(driver);
        },30000
    );

});
