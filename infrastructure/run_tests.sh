#!/bin/bash -eux
cat /terraform/*.pem > /terraform/fullchain.pem
#cat /terraform/fullchain.pem >> /opt/conda/ssl/cert.pem #Default python certs 
cat /etc/ssl/certs/ca-certificates.crt >> /terraform/fullchain.pem
export REQUESTS_CA_BUNDLE=/terraform/fullchain.pem
echo "CONFIG_PATH: $CONFIG_PATH"
if [ -n "$CONFIG_PATH" ] # No CONFIG specified. We will obtain config from Terraform.
     then
     if [ -z "$TEST_ENV" ]
      then
         echo 'If CONFIG_PATH is set, then must set TEST_ENV'
         exit 1
     fi
  else
    echo 'Getting config from terraform'
    export CONFIG_PATH=$(pwd)/tac_configuration/config.json
    export TEST_ENV=$(terraform output project_id)
    mkdir -p ./tac_configuration/
    terraform output json_config > ${CONFIG_PATH}
    declare -r APVM=$(terraform output apvm_web_ui)
    declare -r DOVM=$(terraform output dovm_web_ui)

    #Waits for frontend containers to come up, waits up to 5 minutes (300s)
    wait-for-url() {
        echo "Testing $1"
        timeout -s TERM 300 bash -c \
        'while [[ "$(curl  --cacert /terraform/fullchain.pem -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]];\
        do echo "Waiting for ${0}" && sleep 2;\
        done' ${1}
        echo "OK!"
        curl -I $1
    }

    wait-for-url ${APVM}
    wait-for-url ${DOVM}

    export SITE_URL_APVM=${APVM}:8082
    export SITE_URL_APVM_NO_MARKET=${APVM}
    export SITE_URL_DOVM=${DOVM}
    echo "SITE_URL_APVM: $SITE_URL_APVM"
    echo "SITE_URL_APVM_NO_MARKET: $SITE_URL_APVM_NO_MARKET"
    echo "SITE_URL_DOVM: $SITE_URL_DOVM"
fi



pushd ..
    cd /terraform/ui-test && npm install
    echo 'list============================ '
    ls -la && pwd
    echo 'list==================END========== '
    if [ -n "$TEST_TO_EXECUTE" ]
     then
      echo 'Executing test : '$TEST_TO_EXECUTE
      npm run test:all
      result=$?
     else
      echo 'Running all test'
      npm run test:all
      result=$?
    fi
popd

echo 'result = '$result

if [ -z "$CONFIG_PATH" ];   # if running in codeship mode and it fails, get logs
  then
  if [ "$result" -ne "0" ];
   then
     echo 'Retrieving logs'
     source ./get_logs.sh
  fi
fi

exit $result
