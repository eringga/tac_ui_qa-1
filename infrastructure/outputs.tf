output "apvm_private_ip" {
  value = module.tac-network.apvm_private_ip
}

output "dovm_private_ip" {
  value = module.tac-network.dovm_private_ip
}

output "json_config" {
  description = "Connect to your TAC system with the following JSON config."
  value       = module.tac-network.json_config
}

output "config_s3_path" {
  description = "The path to download your s3 config"
  value       = module.tac-network.config_s3_path
}

output "dovm_web_ui" {
  value = module.tac-network.dovm_web_ui
}

output "apvm_web_ui" {
  value = module.tac-network.apvm_web_ui
}

output "project_id" {
  value = module.tac-network.project_id
}