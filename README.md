# TAC_UI_QA READ ME
   
This repository contains files for end to end testing of the TAC.  

## Workflow

1. Clone this repo and open a feature branch from master of the form `feature/<foo>`. **IMPORTANT** Make sure that the branch name (including feature/) is less than 39 characters and does not contain underscores.
2. Choose the AMIs you would like to test against by going into `infrastructure/variables.tf` and changing the AMI versions for the respective components. 
3. Commit and push your changes to the branch. If it passes you can open a PR to master to make those versions the new default AMIs to test against. 
4. To better see the logs, you can use jet steps: `jet steps --tag feature --ci-branch <your-branch-name>`

## AMI promotion

The last step of a succesful run of TAC_QA on master branch will trigger the AMI promotion process which:

    1. Closes the source feature branches used to generate the AMIs

    2. Converts the AMIs into a "golden version"

See [the image promotion repo](https://bitbucket.org/viascience_dev/lambda-image-versions/src/master/) for more documentation on this process.

## Run a single test using TAC_QA using Terraform-backend managed infrastructure 

1. Create the `.env` file from the `.env.example`

2. Run `npm install` in  `ui-test` directory.

3. `jet steps --key-path viascience_dev_tac_ui_qa.aes --tag feature/branchname --ci-branch feature/branchname -e TEST_TO_EXECUTE=my_test_name` 
    - Note: This will pass the *TEST_TO_EXECUTE* env var to *pytest* and only run a specific test.

4. To only run a single test using Codeship, you can specify the TEST_TO_EXECUTE environment variable in *codeship-services.yml* 
    - To do this, add a *environment* section to the *run-integration-tests* and specify like:
    

       `
       environment:
         - TEST_TO_EXECUTE=test_ael_get_fake_data
       `
      

## Run TAC_QA using jet with a local TAC network config file

1. Create the `.env` file from the `.env.example`

2. Run `npm install` in  `ui-test` directory.

3. `jet steps --key-path viascience_dev_tac_ui_qa.aes --tag run-test -e CONFIG_PATH=./infrastructure/tac_configuration/config.json -e TEST_ENV=env_name` 
    - Note: This will run all the tests.
    - Note: You can also provide the environment variables above using the *codeship-services.yml*.

4. `jet steps --key-path viascience_dev_tac_ui_qa.aes --tag run-test -e CONFIG_PATH=./infrastructure/tac_configuration/config.json -e TEST_ENV=env_name -e TEST_TO_EXECUTE=my_test_name` 
    - Note: This will pass the *TEST_TO_EXECUTE* env var to *pytest* and only run a specific test.
    - Note: You can also provide the environment variables above using the *codeship-services.yml*.
    


## Run TAC_QA using Docker

1. `docker build . -t tac_ui_qa:latest 
    - Note: you can get both the conda API and Gemfury keys from the encrypted build arguments or AWS Secret Manager.
    

2. `docker run -e CONFIG_PATH=$(pwd)/tac_configuration/config.json -e TEST_ENV=$TEST_ENV -w $(pwd) tac_qa:latest bash -c 'python setup.py test'`
    - Note: `$TEST_ENV` is one of the keys in `tac_configuration`, eg. staging
    - Note: `CONFIG_PATH` is the location to the tac_configuration json file
    - Note: You can run a single test file using pytest. Change `python setup.py test` to `pytest path/to/test_file.py`

## List of integration tests and status (TAC tab)

https://docs.google.com/document/d/1S5HqTL2MwLHQTqUsP_aBqIlj0h8L8fL_rUG596w9T0w/edit?usp=sharing



# Who do I talk to? #

* Maintainer: Geeth
* Jesus

